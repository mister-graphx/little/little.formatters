# Little Formatters

Ce package propose une Interface et classe de chargements de formatters.
Les formatters étant des librairies définissant une syntaxes simplifiant la mise en forme des textes et contenus afin de produire du html.


## Formatters inclus

- Markdown : ParseDownExtra
- MarkdownClassic : Michel F. Markdown
- MarkdownExtra : Michel F. MarkdownExtra

## Service config


```yaml
# Default formatter
formatter: 'Markdown'
# Formatters list
formatters:
    Markdown: 'Little\Formatters\Providers\ParseDownExtraFormatter'
    MarkdownClassic:       'Little\Formatters\Providers\MarkdownFormatter'
    MarkdownExtra:  'Little\Formatters\Providers\MarkdownExtraFormatter'

```
