<?php namespace Little\Formatters\Interfaces;
/**
 * Formatters interface
 *
 * rendering helpers for contents
 *
*/
interface FormatterInterface {
    /**
     * Render method for the formatter (parsedown, textile, markdownExtra)
     *
     * @param string $flux the content to be formated
    */
    public function format($flux);

}
