<?php namespace Little\Formatters\Providers ;

use Little\Formatters\Interfaces\FormatterInterface as FormatterInterface;
/**
 * Formatter
 *
 * Generic implementation for the Formatter
 *
 * @todo Ajout les twig filters |markdown
*/
class Formatter implements FormatterInterface {

	/** @var string $formatterName formatter Name */
	public $formatterName;

	/** @var \Little\Kore\templateEngine $templateEngine    Template Engine Object */
	protected $templateEngine;

	public function __construct($templateEngine,$formatterName){
		$this->templateEngine = $templateEngine;
        $this->formatterName = $formatterName;

        $templateEngine->addFilter($this->formatterName,function($str){
            return $this->format($str);
        },['is_safe' => ['all']]);
    }

  /**
	* Render/Format method for the formatter
	*
	* @param string $flux  content to be parsed and transformed
	*/
	public function format($flux){}

    /**
     * Format content
     *
     * Apply formatter to the content, render content as string or inline template
     * return the formatted content or thrown an Exception
     *
     * @param string $content File content
     * @param array $metas    Frontmatter datas
     * @return string|\Exception
     */
	public function formatContent($content,$metas){
		try {
			// Apply the formatter
			$result = $this->format($content);
			// Render meta/twig in content
			$result = $this->templateEngine->renderString($result, $metas);
			return $result;
		} catch(\Exception $e) {
			echo $e->getMessage();
		}
	}

}
