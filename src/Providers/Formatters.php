<?php namespace Little\Formatters\Providers;
/**
 * Formatters loader
 *
 * Provide and load Formatter class
 * depending on the default config, available formatters set from app.yaml
 * or set in the meta content
 *
*/
class Formatters {
    public $formatters; // Formatters list (class|name)
    public $formatter; // Active or default Formater
    protected $templateEngine;

    public function __construct($defaultFormatter,$formatters, $templateEngine){
        $this->templateEngine = $templateEngine;
        $this->formatters = $formatters;
        // Set the default Formatter
        $this->getFormatter($defaultFormatter);
    }
    /**
     * Get Formatter
     *
     * Get in the Formatters array the Class Namespace and the Name
     *
     * @param string $formatterName The formatter name as describe in the yaml config file
    */
    public function getFormatter($formatterName){
        $activeFormatter = $this->formatters[$formatterName];
        if(!in_array($activeFormatter,$this->formatters))
            die($activeFormatter.' is not in Formatters list');
		if(class_exists($activeFormatter)){
			return $this->formatter = new $activeFormatter($this->templateEngine,$formatterName);
        }else{
            die($activeFormatter.' is not a valid class');
        }
    }

}
