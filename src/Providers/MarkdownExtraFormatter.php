<?php namespace Little\Formatters\Providers ;

use Michelf\MarkdownExtra;
/**
 * MarkDown Extra Formatter
 *
 * @see https://michelf.ca/projets/php-markdown/configuration/
 * @todo add config options for markDwown Extra
*/
class MarkdownExtraFormatter extends Formatter {
	public $formatterName;

    public function format($content){
		return MarkdownExtra::defaultTransform($content);
	}
}
