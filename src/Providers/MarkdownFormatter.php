<?php namespace Little\Formatters\Providers ;

use Michelf\Markdown;
/**
 * MarkDown Formatter
 *
 * @see https://michelf.ca/projets/php-markdown/configuration/
 * @todo add config options for markDwown
*/
class MarkdownFormatter extends Formatter {
	public $formatterName;


    public function format($content){
		return Markdown::defaultTransform($content);
	}
}
