<?php namespace Little\Formatters\Providers ;

use ParsedownExtra;
/**
 * ParseDown Extra Formatter
 *
 * @see https://github.com/erusev/parsedown-extra
 *
*/
class ParseDownExtraFormatter extends Formatter {
	public $formatterName;

    public function format($content){
		return ParsedownExtra::instance()->text($content);
	}
}
